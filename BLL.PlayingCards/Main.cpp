// C++ Assignment 3 Playing Cards
// Bret L Lewis and Edited By John Riley

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank
{
	TWO = 2,
	THREE,
	FOUR, 
	FIVE,
	SIX,
	SEVEN, 
	EIGHT,
	NINE,
	TEN,
	ELEVEN, //JACK
	TWELVE, //QUEEN
	THIRTEEN, //KING
	FOURTEEN //ACE
};

enum Suit
{
	SONE = 1, //HEARTS,
	STWO, //DIAMONDS,
	STHREE, //SPADES,
	SFOUR, //CLUBS
};

struct Card
{
	Suit suit;
	Rank rank;
};


void PrintCard(Card card) {
	
	
	switch(card.rank) {
		case TWO:
			cout << "Two Of ";
			break;
		case THREE:
			cout << "Three Of ";
			break;
		case FOUR:
			cout << "Four Of ";
			break;
		case FIVE:
			cout << "Five Of ";
			break;
		case SIX:
			cout << "Six Of ";
			break;
		case SEVEN:
			cout << "Seven Of ";
			break;
		case EIGHT:
			cout << "Eight Of ";
			break;
		case NINE:
			cout << "Nine Of ";
			break;
		case TEN:
			cout << "Ten Of ";
			break;
		case ELEVEN:
			cout << "Jack Of ";
			break;
		case TWELVE:
			cout << "Queen Of ";
			break;
		case THIRTEEN:
			cout << "King Of ";
			break;
		case FOURTEEN:
			cout << "Ace Of ";
			break;
	}

	switch (card.suit) {
		case SONE:
			cout << "Hearts.";
			break;
		case STWO:
			cout << "Diamonds.";
			break;
		case STHREE:
			cout << "Spades.";
			break;
		case SFOUR:
			cout << "Clubs.";
			break;
	}

}

Card HighCard(Card c1, Card c2) {
	if (c2.rank > c1.rank) {
		cout << "Card 2 is bigger";
	}
	else
	{
		cout << "Card 1 is bigger";
	}
	return c1, c2;
}



int main()
{
	Card c1;
	Card c2;

	c1.rank = FOURTEEN;
	c1.suit = STHREE;

	c2.rank = FOUR;
	c2.suit = STWO;

	

	HighCard(c1, c2);


	_getch();
	return 0;
}